Miscellaneous WMF Fundraising tools. Handling **public, non-sensitive** data only!

# diff

Static diff tool for comparing code or previews of two CentralNotice banners e.g. https://tools-static.wmflabs.org/fundraising-tools/diff.html?banner1=B1920_121018_en6C_dsk_p2_sm_txt_cnt&banner2=B1920_121018_en6C_dsk_p2_sm_txt_donack1 Uses https://www.mediawiki.org/wiki/API:Compare

Hosted on toolforge static, symlink located in /www/static/
